import { AngularclassPage } from './app.po';

describe('angularclass App', function() {
  let page: AngularclassPage;

  beforeEach(() => {
    page = new AngularclassPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
